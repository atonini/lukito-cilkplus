/* 
 Autor: Aline Tonini
 Implementação do problema do Menor Caminho 
 */ 

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>
#include <vector>
#include <cilk/cilk.h>
#include <boost/algorithm/string.hpp>
#include "Grafo.hpp"


using namespace std;
using namespace boost;

int main(int argc, char *argv[]){
    
    int n=0, source=0;
 
    if (argc>1){
        n = atoi(argv[1]);
        source= atoi (argv[2]);        
    }else {
        cout << "passagem de parametros invalida!";
        return 1; 
    }
    
    int origem, destino, custo;
    Grafo * G  = new Grafo(n);
      
    string line;
    ifstream entrada ("entrada.txt");
    int ret=0;
    
    if (entrada.is_open()){

        while (! entrada.eof() ){

            getline (entrada,line); 
            vector<string> str_split;
            split(str_split,line,is_any_of(" ")); 
            origem = atoi(str_split[0].c_str());
            destino= atoi(str_split[1].c_str());
            custo= atoi(str_split[2].c_str());
            
            ret=cilk_spawn(G->inserir_elemento(origem,destino,custo));
            
            if(ret==1){ 
                cout << "ERRO ao inserir elemento no grafo! \n" ;
                return 1;
            }
            
        }
        entrada.close();
        cilk_sync;

     } else {
        cout << "Unable to open file"; 

    }
    
    G->print_grafo();
    G->menor_caminho(source);
        
    return 0;
    
}
