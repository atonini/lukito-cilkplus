#####################################################################################################################
#                   Lukito-CilkPlus  - Trabalho I de Introduçao ao Processamento Paralelo e Distribuído		    #
#####################################################################################################################
#
#   1. Introduçao
#   2. Implementação 
#   3. Execução
#   
#   @autor Aline Rodrigues Tonini
#   @version 1.0
# 
#####################################################################################################################
#   1. Introduçao												    #
#####################################################################################################################
#
#   O projeto Lukito se propõem a realizar o trabalho I da disciplina de IPPD, proposto pelo Prof. Gerson Cavalheiro.
#   O trabalho proposto é a implementação do algoritmo de busca para o menor caminho (Single Source Shortest Path)
#   com uma ferramenta de programação concorrente. Neste caso foi utilizado a ferramenta CilkPlus e o algoritmo usado
#   é uma variação do algoritmo de Bellmann-Ford para menores caminhos. 
#
#####################################################################################################################
#   2. Implementação												    #
#####################################################################################################################
#
#    Na implementação do projeto foi utilizado a linguagem C++, a biblioteca boost para a leitura do arquivo
#    de entrada e a ferramenta CilkPlus com o compilador Intel® C++ Composer XE com licensa para estudante.
#    O trabalho foi desenvolvido utilizando a IDE Netbeans e o versionador Bitbucket para manter o código organizado. 
#
#    Na implementação foi criada uma classe grafo onde o grafo foi implementado com uma matriz de adjacencias.
#    O grafo é direcionado e o valor da matriz é o custo. Não são permitidos custos negativos nesta versão
#    do algoritmo de Bellmann-Ford. Por exemplo origem->1, destino->2, custo->5 é implementado desta forma :
#				grafo[1][2]=5
#
#    Ao ser executado o programa  é passado o número de vértices do grafo.
#    Onde 0 é o valor mínimo para o "nome" de um nodo e n é o valor máximo.
#
#    A concorrencia é implementado da seguinte forma: apenas em loops que não alteram o estado do objeto grafo,
#    na função de inserção das entradas no grafo e na busca por vizinhos no algortimo.
#    A primitiva cilk_for é utilizada para inicialização do vetor de distâncias e na busca por vizinhos no algoritmo.
#    A primitiva cilk_spawn é utilizada na para realizar a inserção da entrada no grafo, enquanto outra entrada é lida.
#    A primitiva cilk_sync é utilizada para sincronizar as tarefas antes de realizar a busca pelo menor caminho.
#
#####################################################################################################################
# 	3. Execução												    #
#####################################################################################################################
#
#	Para executar é necessário possuir o arquivo Main.cpp, Grafo.cpp, Grafo.hpp, Makefile e o arquivo entrada.txt.
#	Além destes arquivos a biblioteca Boost deve estar instalada**.
#
#	O arquivo entrada.txt já contém a entrada do grafo, cada linha do arquivo representa uma entrada, que está
# 	no formato "origem destino custo".
#	Por exemplo:
#           	1 2 5  origem=1 destino=2 custo=5
#
#	Após o Makefile, execute passando como parametro o número de nodos e o nodo de origem.
#	 			./exec NUM_NODOS NODO_ORIGEM
#
#	O nome do arquivo de entrada está setado no código, se mudar o nome, este deve ser alterado no código.
#
#	**Caso ocorra algum erro por seu computador não conter a biblioteca boost instalada, instale usando
#	 Fedora : yum install boost-devel
#	 Ubuntu : sudo apt-get install libboost-all-dev
#
##################################################################################################################### 