# Autor : Aline Tonini
# Makefile
#

CC=icpc
CFLAGS=-c
LDFLAGS= -lboost_system -I /usr/include/boost/algorithm
SOURCES= Main.cpp Grafo.cpp
OBJECTS= $(SOURCES:.cpp=.o)
EXECUTABLE=exec

all: $(EXECUTABLE)	

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $(EXECUTABLE)

%.o: %.cpp 
	$(CC) $(CFLAGS) $(SOURCES)

clean:
	rm -rf *o exec